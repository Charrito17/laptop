package Laptop;

public class Lenovo extends Laptop {
	// This is where i entered my field
	String Computer;

	// We must add this blank constructor which reads the in Laptop that blank
	// constructor
	public Lenovo() {
		super();
		this.Computer = "Windows";
	}

	// This constructor now reads the input in place that we have inside of Main
	public Lenovo(double price, double weight, double screenSize, String model) {

		this.price = price;
		this.weight = weight;
		this.screenSize = screenSize;
		this.Computer = "Windows";

	}

	// this display now tied to the Laptop prints everything we need in Main.
	public void display() {
		System.out.println("()()()()()()()()()()()()()()()()");
		super.display();
		System.out.println("Lenovo: \t" + Computer);
		System.out.println("()()()()()()()()()()()()()()()()");

	}

}
