package Laptop;

public class Mac extends Laptop {
	String type;

	// constructor reads the material from main so that it inputs it.
	public Mac(double price, double weight, double screenSize, String model) {

		this.type = "air";
		this.price = price;
		this.weight = weight;
		this.screenSize = screenSize;
	}

	// this is the blank one again that reads the manual blank one in laptop
	public Mac() {
		super();

	}

	// method
	@Override
	public void display() {
		System.out.println("()()()()()()()()()()()()()()()()");
		super.display();
		System.out.println("Macbook: \t" + type);
		System.out.println("()()()()()()()()()()()()()()()()");

	}

}